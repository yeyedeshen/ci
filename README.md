# CI

### 接口

- /swagger
  接口文档
- /sn
  获取sn号
- /hap-build/PRId
  build对应项目

### 项目配置

- 须放置applications_app_samples文件夹与项目同级
- 获取修改的代码和build部分尚未整合，调用/hap-build/PRId，其实是直接调用流水线shell脚本，会拉取更新库上最新代码并且编译
- `config.dev.ts` 为配置文件路径