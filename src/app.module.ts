import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { HapBuildModule } from './hap-build/hap-build.module';

@Module({
  imports: [HapBuildModule],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
