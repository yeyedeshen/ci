import { Controller, Get, Post, Body, Patch, Param, Delete } from '@nestjs/common';
import { HapBuildService } from './hap-build.service';
import { CreateHapBuildDto } from './dto/create-hap-build.dto';
import { UpdateHapBuildDto } from './dto/update-hap-build.dto';
import { getModifyFilelist } from './utils/getModifyFileList';
import { ApiOperation, ApiTags } from '@nestjs/swagger';

@ApiTags('编译App hap')
@Controller('hap-build')
export class HapBuildController {
  constructor(private readonly hapBuildService: HapBuildService) { }

  @Get(':id')
  getFilelist(@Param('id') PRId: number) {
    return getModifyFilelist(PRId);
  }
}
