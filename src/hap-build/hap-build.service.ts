import { Injectable } from '@nestjs/common';
import { CreateHapBuildDto } from './dto/create-hap-build.dto';
import { UpdateHapBuildDto } from './dto/update-hap-build.dto';

@Injectable()
export class HapBuildService {
  create(createHapBuildDto: CreateHapBuildDto) {
    return 'This action adds a new hapBuild';
  }

  findAll() {
    return `This action returns all hapBuild`;
  }

  findOne(id: number) {
    return `This action returns a #${id} hapBuild`;
  }

  update(id: number, updateHapBuildDto: UpdateHapBuildDto) {
    return `This action updates a #${id} hapBuild`;
  }

  remove(id: number) {
    return `This action removes a #${id} hapBuild`;
  }
}
