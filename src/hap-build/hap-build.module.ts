import { Module } from '@nestjs/common';
import { HapBuildService } from './hap-build.service';
import { HapBuildController } from './hap-build.controller';

@Module({
  controllers: [HapBuildController],
  providers: [HapBuildService],
})
export class HapBuildModule {}
