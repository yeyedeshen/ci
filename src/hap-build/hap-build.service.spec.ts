import { Test, TestingModule } from '@nestjs/testing';
import { HapBuildService } from './hap-build.service';

describe('HapBuildService', () => {
  let service: HapBuildService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [HapBuildService],
    }).compile();

    service = module.get<HapBuildService>(HapBuildService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
