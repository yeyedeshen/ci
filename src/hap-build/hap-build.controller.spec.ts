import { Test, TestingModule } from '@nestjs/testing';
import { HapBuildController } from './hap-build.controller';
import { HapBuildService } from './hap-build.service';

describe('HapBuildController', () => {
  let controller: HapBuildController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [HapBuildController],
      providers: [HapBuildService],
    }).compile();

    controller = module.get<HapBuildController>(HapBuildController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
