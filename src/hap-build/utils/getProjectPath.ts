import { buildListProject } from "./buildListProject";
import { PROJECT_PATH } from "config.dev";

function getProjectPath(PRId: number, modifyPath: Array<string>) {
    let projectPath = [];
    const fs = require('fs');
    modifyPath.forEach((item: string) => {
        let iteraterArray = item.split('/');
        let pathString = '';
        for (let i = 0; i < iteraterArray.length; i++) {
            if (!fs.existsSync(PROJECT_PATH + pathString + '/build-profile.json5')) {
                pathString = `${pathString}/${iteraterArray[i]}`;
            } else if (!projectPath.includes(pathString.substring(1))) {
                pathString = pathString.substring(1);
                projectPath.push(pathString);
                break;
            }
        }
    })
    console.log('PR修改文件的项目路径是： ');
    console.log(projectPath);
    return buildListProject(PRId, projectPath)
}

export { getProjectPath }