import { CONTAINS_PATH, SIGN_FULL_PATH, SIGN_PUBLIC_PATH } from 'config.dev';
import { checkProjectIfInFull } from "./checkProjectIfInFull"
const fs = require('fs');

interface ContainsType {
    SIGN_HAP_PATH?: string;
    FA_MODAL_AND_LOWER_CASE_LIST?: string;
    INSTALL_LIST_CAPABILITY?: string;
    SAVE_XML_PATH?: string;
    COMBIN_CONFIG?: string;
    SPECIAL_LIST?: string;
    SPECIAL_HAP?: string;
    TARGET_PATH?: string;
}

function editLinuxContains(targetPath: string) {
    try {
        const config: ContainsType = {};
        const configContent = fs.readFileSync(CONTAINS_PATH, 'utf8');
        configContent.trim().split('\n').forEach((line: string) => {
            if (line.trim() !== '') {
                const [key = '', value = ''] = line.split('=');
                config[key.trim()] = value.trim();
            }
        });
        if (checkProjectIfInFull(targetPath)) {
            config.SIGN_HAP_PATH = `r'${SIGN_FULL_PATH}'`;
        } else {
            config.SIGN_HAP_PATH = `r'${SIGN_PUBLIC_PATH}'`;
        }
        const target = targetPath.split('/').join('_')
        config.TARGET_PATH = `r'${target}'`;
        console.log(Object.entries(config).length);
        const updatedConfigContent = Object.entries(config)
            .map(([key, value]) => `${key} = ${value}`)
            .join('\n');
        fs.writeFileSync(CONTAINS_PATH, updatedConfigContent);
        return {
            status: true,
            msg: ''
        }
    } catch (e) {
        return {
            status: false,
            msg: JSON.stringify(e)
        }
    }
}

export { editLinuxContains }