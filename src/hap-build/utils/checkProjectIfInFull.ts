import { CONFIG_PATH } from 'config.dev';
const fs = require('fs');

interface ConfigType {
    name?: string,
    url?: string,
    branch?: string,
    fullSdkAssembleList?: string,
    basicSignList?: string,
    coreSignList?: string
    systemAppList?: string
}

function checkProjectIfInFull(projectName: string) {
    try {
        const config: ConfigType = {};
        const configContent = fs.readFileSync(CONFIG_PATH, 'utf8');
        configContent.split('\n').forEach((line: string) => {
            if (line.trim() !== '') {
                const [key = '', value = ''] = line.split('=');
                config[key.trim()] = value.trim();
            }
        });
        const fullSdkAssembleList: string[] = config.fullSdkAssembleList.split(';');
        return fullSdkAssembleList.includes(projectName);
    } catch (e) {
        return false;
    }
}

export { checkProjectIfInFull }