import { PartialType } from '@nestjs/mapped-types';
import { CreateHapBuildDto } from './create-hap-build.dto';

export class UpdateHapBuildDto extends PartialType(CreateHapBuildDto) {}
